#include <boost/archive/text_oarchive.hpp>
#include <boost/lambda/lambda.hpp>
#include <iostream>
#include <iterator>
#include <algorithm>

int main()
{
    using namespace boost::lambda;
    typedef std::istream_iterator<int> in;

    std::for_each(
        in(std::cin), in(), std::cout << (_1 * 3) << " " );
}
/*
#include <boost/archive/text_oarchive.hpp>

#include <serialization/list.hpp>
#include <serialization/string.hpp>
#include <serialization/version.hpp>
#include <serialization/split_member.hpp>
#include <iostream>

/////////////////////////////////////////////////////////////
// gps coordinate
//
// illustrates serialization for a simple type
//

class gps_position{
private:
    friend class boost::serialization::access;
    // When the class Archive corresponds to an output archive, the
    // & operator is defined similar to <<.  Likewise, when the class Archive
    // is a type of input archive the & operator is defined similar to >>.
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & degrees;
        ar & minutes;
        ar & seconds;
    }
    int degrees;
    int minutes;
    float seconds;
public:
    gps_position(){};
    gps_position(int d, int m, float s) :
        degrees(d), minutes(m), seconds(s)
    {}
};


int main() {
    // create and open a character archive for output
    //std::ofstream ofs("filename.txt",std::ofstream::out);
    const gps_position g(35, 59, 24.567f);
    boost::archive::text_oarchive oa(std::cout);
    int i = 1;
    oa << i;
    
    // create class instance
 
        
    
        // save data to archive
     
    //boost::archive::text_oarchive oa(ofs,0);
            // write class instance to archive
    //oa << g;
        	// archive and stream closed when destructors are called
        
    
        // ... some time later restore the class instance to its orginal state
    //gps_position newg;
        /*
            // create and open an archive for input
            std::ifstream ifs("filename");
            boost::archive::text_iarchive ia(ifs);
            // read class state from archive
            ia >> newg;
            // archive and stream closed when destructors are called
        
    
    return 0;
}
*/