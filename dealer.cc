#include <czmq.h>

#include <iostream>     // std::cout
#include <algorithm>    // std::make_heap, std::pop_heap, std::push_heap, std::sort_heap
#include <vector> 
#include <set>
#include <iostream>
#include <utility>
#include "format.h"
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <set>
#include <ctype.h>
#include <ctime>
#include "math.h"


#include <fstream>

using std::vector;
using std::set;
using std::pair;
using std::string;
using std::cout;
using std::endl;
using std::set;
using fmt::print;
using std::push_heap;
using std::make_heap;
using std::pop_heap;

using Coordinate = pair<size_t, size_t>; //Se utiliza para tener la cordenas de la actual celda. fil, col
using Cell = set<int>;

string intToS(int n)
{
std::ostringstream sin;
sin << n;
std::string val = sin.str();
return val;
}

struct Statistics {
  size_t solutions;
  size_t failures;
  size_t decisions;
  size_t reductions;
  int solutionsLast;

  Statistics()
      : solutions(0)
      , failures(0)
      , decisions(0)
      , reductions(0)
      , solutionsLast(0) {}

  void print() const {
    fmt::print_colored(
        fmt::GREEN,
        "Solutions: {}\t Failures: {}\t Decisions: {}\t Reductions: {}\n",
        solutions, failures, decisions, reductions);
  }
  
  void record_solutions(double timestamp){
      int x=timestamp;
      //int solutions_it=solutionsLast;
     

       if(x%10==0 && x!=0){
         cout<<"new timestamp"<<endl;
         std::ofstream ofs("record_solutions.txt",std::ofstream::out);
         
         //int resta=solutions_it-solutionsLast;
         ofs <<"timestamp: "<<timestamp<<endl<<"Total Solutions: "<<solutions<<" Failures: "<<failures<< " Decisions: "<< decisions << " Reductions: "<< reductions <<endl;
        ofs.close();


      }

      


    }
};


////////////////////////////////////////////////////////////////////////////////

class Sudoku {
private:
using Board = vector<vector<Cell>>;
string board_string;


Board board;

public:
Sudoku() {
  Cell all{1, 2, 3, 4, 5, 6, 7, 8, 9};
  board = {
      {all, all, all, all, all, all, all, all, all},
      {all, all, all, all, all, all, all, all, all},
      {all, all, all, all, all, all, all, all, all},
      {all, all, all, all, all, all, all, all, all},
      {all, all, all, all, all, all, all, all, all},
      {all, all, all, all, all, all, all, all, all},
      {all, all, all, all, all, all, all, all, all},
      {all, all, all, all, all, all, all, all, all},
      {all, all, all, all, all, all, all, all, all},
  };
}



Sudoku(string sk){
  std::string::iterator it;
  int i=0;
  int j=0;
  int k=1;
  Cell c;
  vector <Cell> col;
  Board b;
  for(it=sk.begin(); it!= sk.end(); it++){
      
      char space='\n';
      char number=*it;
      
      if(space==number){// nueva celda
         //c.erase (c.begin(), c.end());
         //cout<<c.size()<<endl;
         col.push_back(c);
         c.erase(c.begin(),c.end());
         
         if(k%9==0){
           //cout<<"nueva fila"<<endl;
           b.push_back(col);
           col.erase(col.begin(), col.end());
           i++;
         }
         
         k++;
      }else{//es un digito
        int b=number - '0';
        c.insert(b);
      }
    }
  
  board=b;
  //cout<<i<<":"<<j<<":"<<k<<endl;
}


/*
 * Ininicializa el soduku si la casilla es igual a 0 coloca un set de datos all{1,2,3,4,5,7,8,9}
*/

    Sudoku(const vector<vector<int>>& s) {
      Cell all{1, 2, 3, 4, 5, 6, 7, 8, 9};
      for (const auto& row : s) {
        board.push_back(vector<Cell>());
        for (const auto& v : row) {
          if (v == 0)
            board.back().push_back(all);
          else
            board.back().push_back({v});
        }
      }
    }
    
    
    
    Sudoku(const Sudoku& other)
        : board(other.board) {}
        
    void print() const {
      fmt::print("Sudoku\n");
      size_t i = 0;
      for (const auto& row : board) {
        size_t j = 0;
        if (i % 3 == 0) fmt::print("{:-^13}\n", "");
        for (const auto& cell : row) {
          if (j % 3 == 0) fmt::print("|");
    
          if (cell.size() == 0) {
            fmt::print_colored(fmt::RED, "{{}}");
          } else if (cell.size() == 1) {
            auto elem = *(cell.cbegin());
            fmt::print_colored(fmt::GREEN, "{}", elem);
          } else {
            fmt::print("{{");
            for (auto i : cell) fmt::print("{},", i);
            fmt::print("}}");
          }
          j++;
        }
        fmt::print("|\n");
        i++;
      }
      fmt::print("{:-^13}\n", "");
    }
    
  ///////////////////////////////SERIALIZER/////////////////////////////////////
    
    string serializer()const{
      string str="";         
      
      for(int i=0; i<9;i++){
        for(int j=0; j<9; j++){
		 Cell c=board[i][j];
		 
         
          for(std::set<int>::iterator it=c.begin(); it!= c.end(); it++){
            //int value= *it; //no necesito la representación numerica ya que esta es de tamañano de 32bits            
			char *msg;
			msg=(char*) malloc (sizeof(char));
			sprintf(msg, "%d", *it);
            str.push_back(msg[0]);
            free (msg);
          } 
          
          str.push_back('\n');
        }
      }
      
      return str;
    }

    
       Cell deserializerLastCell(string cell_in){
          Cell cell_out;
          
          std::string::iterator it;
                   
          for (it=cell_in.begin(); it!=cell_in.end(); it++){
                char number=*it; 
                int d=number - '0'; //De esta forma se puede obtener un digito si number es u caracter
                cell_out.insert(d);
          }
          
          return cell_out;
        }

    
  //////////////////////////////////////////////////////////////////////////////  
    Board getBoard(){
      return board;
    }
    
    
    string stringSudokuFile_in(string fileName){
        
       string sudoku;
       std::ifstream ifs(fileName,std::ifstream::in);
       char c = ifs.get();
    
        while (ifs.good()) {
          sudoku.push_back(c);
          c = ifs.get();
        }
       ifs.close();
       return sudoku;
       
    }
    
    void stringSudokuFile_out(string fileName,string data_out){
        std::ofstream ofs(fileName,std::ofstream::out);
        ofs << data_out;
        ofs.close();
    }
    
    void removeValueForCell(const Coordinate& c, int v) {
      board[c.first][c.second].erase(v);
    }
    
    void setValuesForCell(const Coordinate& c, Cell values){
       board[c.first][c.second]=values;
    }

    Cell getCell(const Coordinate& c){
      return board[c.first][c.second];
    }

};
    /*
     ____________:::::::::::::::__END CLASS SUDOKU__:::::::::::::::_____________
    */

class process{
private:
int priority;
int val;
Cell valuesLastCell;
Coordinate c;
Sudoku s;
bool finished_process;
  
public:
  process(void){
   priority=0;
   val=-1;
   c={0, 0};
   finished_process=false;
  }
  
  process(Sudoku _s){
   priority=0;
   val=-1;
   c={0, 0};
   finished_process=false;
   s=_s;
  }
  
  
  process(int _priority,int _val,Coordinate _c, Sudoku _s){
     priority=_priority;
     val=_val;
     c=_c;
     s=_s;
     valuesLastCell=s.getCell(c);
     finished_process=false;
  }
  
  //////////////////////////////// SERIALIZER ///////////////////////////////////
  
      string serializerLastCell(){
          Cell cell_in=valuesLastCell;
          string cell_out;
          std::set<int>::iterator it;
          
          for (it=cell_in.begin(); it!=cell_in.end(); it++){       
            
            char *c;
			c=(char*) malloc (sizeof(char));
			sprintf(c, "%d", *it);
            cell_out.push_back(c[0]);
            free (c);            
              
          }
          
          return cell_out;
      }
        
    
  ////////////////////////////////// SETs AND GETS /////////////////////////////
      int getVal(){
        return val;
      }
      
      Cell getValuesLastCell(){
        return valuesLastCell;
      }
      
      Coordinate getCoordinate(){
        return c;
      }
      
      Sudoku getSudoku(){
        return s;
      }
      
      void setSudoku(Sudoku _s){
         s=_s;
      }
      
      bool isFinished(){
        return finished_process;
      }
      
      int getPriority(){
        return priority;
      }
      
      void setPriority(int pr){
        priority=pr;
      }
      
      bool getFinished_process(){
        return finished_process;
      }
      
      void setFinished_process(bool val){
        finished_process=val;
      }
};
    /*
     ____________:::::::::::::::__END CLASS PROCESS__:::::::::::::::_____________
    */






class worker{
private:
 zframe_t* identity;
 int idWorker;
 process currentProcess;
 bool be_free; //esta a la espera por un proceso
 
 
public:
  worker(void):identity(nullptr){}
  
  worker(zframe_t* _identity,int _idWorker){
    identity = zframe_dup(_identity); 
    idWorker=_idWorker;
    be_free=true;
  }
  
  void setIdentity(zframe_t* _identity){
    identity = zframe_dup(_identity);
    be_free=true;
  }
  
  void setFreeProcess(){
    be_free=true;

  }
  
  
  void  setCurrentProcess(process _p){
    
    currentProcess=_p;
    be_free=false;
  }
  
  process getCurrentProcess(){
    return currentProcess;
  }
  
  bool isFree(){
    return be_free;
  }
  
  zframe_t* getIdentity(){
    return identity;
  }
  
  int getIdWorker(){
    return idWorker;
  }
  

};

    /*
     ____________:::::::::::::::__END CLASS WORKER__:::::::::::::::_____________
    */



//Colocarle cuidado a esta parte
class timmer{
  public:
   std::clock_t start;
   double duration;
  
  timmer(void){
  }
  
  void start_time(){
    duration=0.0;
    start=0;
  }  
  
  
  void printDurationExection(){
    duration = ( std::clock() - start )/(double) CLOCKS_PER_SEC;
    std::cout<<"Time Exection: "<< duration <<" seconds"<<'\n';
  }
  
  double getDuration(){
    return duration;
  }
  
};


template <typename size_t, typename Foo>
bool comparePairs(const std::pair<size_t,process>& lhs, const std::pair<size_t,process>& rhs)
{
  return lhs.first <= rhs.first;
}



class  dealerState{
      private:
        vector <worker> worker_set; // colección de workers
        vector <pair<size_t,process>> process_set; //Cola de Prioridad  de procesos
        worker solution_worker();  //almaceno el worker solución
        bool finishedDealer;
	string typeSearch;
	bool details=true;
     
      public:
      timmer timeExecution;
      Statistics st;      
      
           
     dealerState(process initial, string _typeSearch, bool _details){
        
          make_heap(process_set.begin(),process_set.end(),comparePairs<size_t,process>);
          push_back_process(initial);
          finishedDealer=false;
	  typeSearch=_typeSearch;
	  details=_details;
	  timeExecution.start_time();
     }
     
      
      void sendMsg(zsock_t* channel, zframe_t* to, vector<string> parts) {
          zmsg_t* msg = zmsg_new();
          zframe_t* dto = zframe_dup(to);
          zmsg_append(msg, &dto);
          for (const string& s : parts) {
            zmsg_addstr(msg, s.c_str());
          }
          zmsg_send(&msg, channel);
      }
      
      void push_back_process(process p){
        process_set.push_back({p.getPriority(),p});
        push_heap (process_set.begin(),process_set.end(),comparePairs<size_t,process>);
      }
      
     pair<int,process> pop_back_process(){
       
       pair<int,process> p=process_set.front();
     
       pop_heap (process_set.begin(),process_set.end(),comparePairs<size_t,process>);
	     process_set.pop_back();
       return p;
      }
      
      
      /*
      *Crea un nuevo worker y lo coloca en el set de workers
      */
      
      void newWorker(zframe_t* identity,int idWorker){
         worker wrk(identity,idWorker);
         worker_set.push_back(wrk);
         assignProcessToWorker(wrk);
      }
      
      
      /*
      * Asigna un proceso utilizando la cola de prioridad, haciendo uso de la estructura heap 
      */
      
      
      void assignProcessToWorker(worker wrk){
        
        if(process_set.size()>0){ 
          
          process p=pop_back_process().second;
          int idWorker=wrk.getIdWorker();
          worker& w=worker_set[idWorker];
          w.setCurrentProcess(p);
          
          
        }else{
         cout<<"empty process";
        }
      }
      
      void setFreeProcess(int idWorker){        
        worker_set[idWorker].setFreeProcess();
     }
      
      
      void actionFindWorkersFree(zsock_t* server){
         for(int i=0; i<worker_set.size(); i++){
             if(worker_set[i].isFree()){
               assignProcessToWorker(worker_set[i]);
               actionSendProcess(worker_set[i].getIdentity(),server, i);
             }
         }
         
      }
       
      void actionJoin(zframe_t* identity,int &idWorker){
      
        if(idWorker==-1){
            int position=worker_set.size();
            idWorker=position;
            newWorker(identity,idWorker);
            cout<<"newWorker:"<<identity<<endl;
         }
         
      }
      
      
      void actionSendProcess(zframe_t* identity, zsock_t* server, int idWorker){
        
            worker wrk=worker_set[idWorker];
            process p=wrk.getCurrentProcess();
            
                if(!wrk.isFree()){ //el worker esta liberado?
                
                  Sudoku s=p.getSudoku();
                  Coordinate c=p.getCoordinate();
                  
                  //cout<<"prioidad en send: "<<p.getPriority()<<endl;
                  
                  string idW= intToS(idWorker);
                  string priority_s= intToS(p.getPriority());
                  string sudoku_s=s.serializer();
                  string val= intToS(p.getPriority());
                 
                  string coordinate_x= intToS(c.first);
                  string coordinate_y= intToS(c.second);
                  string finished_process= (p.getFinished_process())? "1":"0";
                  
                  
                  sendMsg(server, identity, {"solveProcess",idW,priority_s,sudoku_s,val,coordinate_x,coordinate_y,finished_process});
                  
                }
      }
      
      void actionAddProcess(zmsg_t* msg){ 
        
        int priority_in=atoi(zmsg_popstr(msg));
        string sudoku_s_in=zmsg_popstr(msg);
        int result=atoi(zmsg_popstr(msg));
        int val_in= atoi(zmsg_popstr(msg));
        string valuesLastCell_in=zmsg_popstr(msg);//Los valores diferentes a val_in
        int  coordinate_x_in= atoi(zmsg_popstr(msg));
        int  coordinate_y_in= atoi(zmsg_popstr(msg));
    
    
          Coordinate coordinate={coordinate_x_in,coordinate_y_in};
          Sudoku s(sudoku_s_in);// se pasa por parametro el valor del sudoku como un string para luego ser deserializado
       
          if(result==2){
            
            process prsLeft(priority_in+1,val_in,coordinate,s);//Creando Proceso Izquierdo
            
            Cell valuesForCell=s.deserializerLastCell(valuesLastCell_in); 
            
             //De esta forma se construiria el arbol binario
             /*
               s.setValuesForCell(coordinate,valuesForCell);// Colando todos los valores que tenia antes el proceso padre
               process prsRigth(priority_in+1,val_in,coordinate, s); //Creando proceso derecho del sudoku con la celda llena todos 
               push_back_process(prsRigth);
             */
         
                for(const auto& item : valuesForCell){
                  Cell cellV;
                  cellV.insert(item);
                  s.setValuesForCell(coordinate,cellV);// Colando todos los valores que tenia antes el proceso padre
                  process prsRigth(priority_in+1,val_in,coordinate, s); //Creando proceso derecho del sudoku con la celda llena todos 
                  push_back_process(prsRigth);
               }
               
              push_back_process(prsLeft);//le da prioridad al ultimo que ingreso ver ejemplo de heap_clase.cc
              st.decisions++;
        }else{
           finishedDealer=true;
           st.solutions++;
        }       
	  printInfo(s,result);
      }
      
      void printInfo(Sudoku sudoku_s_in, int result){
          if(details){
      	    cout<< "___________________________resolved process___________________________"<<endl;
                  sudoku_s_in.print();
                  cout<<""<<endl;
                  std::cout << "size process:"<< process_set.size() << std::endl;
                  std::cout << "current processes: ";
                   for (unsigned i=0; i<process_set.size(); i++){
                        process po=process_set[i].second;
                        std::cout << " val:"<<po.serializerLastCell()<<""<<" pr:"<< po.getPriority()<<"; ";
                       
                   }
                        
                   cout<<""<<endl;  
                   cout<< "_____________________________________________________________________"<<endl;
      	 
                
                if(result==1){	
                   double durationEx=timeExecution.getDuration();
                   st.record_solutions(durationEx);
                    
      	            cout<< "<<<<<<<<<<<<<<<<<<<<<<< SOLUTION FOUND >>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
                    st.print();
                    timeExecution.printDurationExection();
                }
      	 }else{
      	    if(result==1){
      	       double durationEx=timeExecution.getDuration();
               st.record_solutions(durationEx);
      	       sudoku_s_in.print();
      	      
      	      cout<< "<<<<<<<<<<<<<<<<<<<<<<< SOLUTION FOUND >>>>>>>>>>>>>>>>>>>>>>>>"<<endl;
                    st.print();
                    timeExecution.printDurationExection();
      	      
      	    }
      	 }
           
      }
      
      
      void actionSendNotifyToworkers(zframe_t* identity, zsock_t* server, int idWorker){
        for(int i=0; i<worker_set.size(); i++){
            if(worker_set[i].getIdWorker()!=idWorker){             
             sendMsg(server, worker_set[i].getIdentity(), {"solutionFound",intToS(idWorker)});
            }
          }
      }
      
      
      ///////////////////////////////////GETs AND SETs //////////////////////////////////////
      int getSize(){
        return worker_set.size();
      }
      
      bool isFinishedDealer(){	
      	if(typeSearch=="all"){	  
      	    return false;    
      	}else{
      	  return finishedDealer;
      	}	
      }

};

    /*
     ____________:::::::::::::::__END CLASS DealerState __:::::::::::::::_____________
    */



int handler(zloop_t*, zsock_t* server, void* _dealerState) {
      dealerState * dealer= reinterpret_cast<dealerState*>(_dealerState);
      
      zmsg_t* msg = zmsg_recv(server);
    
      zframe_t* identity = zmsg_pop(msg);
      zframe_t* action = zmsg_pop(msg);
      int  idWorker = atoi(zmsg_popstr(msg));
      

      if(!dealer->isFinishedDealer()){ //sino esta finalizado el  worker
      
          if (zframe_streq(action, "join")) {
              dealer->actionJoin(identity,idWorker);
              dealer->actionSendProcess(identity,server,idWorker);
              
          }else if(zframe_streq(action, "finished_process")){
              
              dealer->st.reductions++;
              dealer->setFreeProcess(idWorker);
              dealer->actionAddProcess(msg);
              dealer->actionFindWorkersFree(server);
              
          }else if(zframe_streq(action, "failed_process")){
            
              dealer->st.failures++;
              zframe_print(identity,"Failed process in :");
              dealer->setFreeProcess(idWorker);
              dealer->actionFindWorkersFree(server);
              
          }
          
      }else{        
        dealer->actionSendNotifyToworkers(identity,server, idWorker);//notifica a todos los workers que ya encontro una solución.
        dealer->st.solutions++;
        
      }
}





int main(){
  
Sudoku a({{0, 0, 3, 0, 2, 0, 6, 0, 0},
      {9, 0, 0, 3, 0, 5, 0, 0, 1},
      {0, 0, 1, 8, 0, 6, 4, 0, 0},
      {0, 0, 8, 1, 0, 2, 9, 0, 0},
      {7, 0, 0, 0, 0, 0, 0, 0, 8},
      {0, 0, 6, 7, 0, 8, 2, 0, 0},
      {0, 0, 2, 6, 0, 9, 5, 0, 0},
      {8, 0, 0, 2, 0, 3, 0, 0, 9},
      {0, 0, 5, 0, 1, 0, 3, 0, 0}});

Sudoku d({
      {0, 3, 0, 0, 5, 0, 0, 4, 0},
      {0, 0, 8, 0, 1, 0, 5, 0, 0},
      {4, 6, 0, 0, 0, 0, 0, 1, 2},
      {0, 7, 0, 5, 0, 2, 0, 8, 0},
      {0, 0, 0, 6, 0, 3, 0, 0, 0},
      {0, 4, 0, 1, 0, 9, 0, 3, 0},
      {2, 5, 0, 0, 0, 0, 0, 9, 8},
      {0, 0, 1, 0, 2, 0, 6, 0, 0},
      {0, 8, 0, 0, 6, 0, 0, 2, 0},
});

  Sudoku c({
      {0, 0, 0, 0, 0, 0, 9, 0, 7},
      {0, 0, 0, 4, 2, 0, 1, 8, 0},
      {0, 0, 0, 7, 0, 5, 0, 2, 6},
      {1, 0, 0, 9, 0, 4, 0, 0, 0},
      {0, 5, 0, 0, 0, 0, 0, 4, 0},
      {0, 0, 0, 5, 0, 7, 0, 0, 9},
      {9, 2, 0, 1, 0, 8, 0, 0, 0},
      {0, 3, 4, 0, 5, 9, 0, 0, 0},
      {5, 0, 7, 0, 0, 0, 0, 0, 0},
  });
  
    Sudoku e({//FATAL SUDOKU
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0},
  });

          
//solve(a);
//Sudoku e(a.stringSudokuFile_in("sudoku_in.txt"));
process work(e);
void *dealer=new dealerState(work,"all",false); //(process p, typeSearch, details)

        /*
         * PRUEBAS DE LA SEREALIZACION CON ARCHIVOS 
        */
         
         // d.stringSudokuFile_out("sudoku_out.txt", d.serializer());
          //cout<<d.stringSudokuFile_in("sudoku_in.txt")<<endl;
         
         /*
          Sudoku b(a.stringSudokuFile_in("sudoku_in.txt"));
          solve(b);
          b.stringSudokuFile_out("sudoku_out.txt", b.serializer());
        */


zsock_t *server = zsock_new(ZMQ_ROUTER);
zsock_bind(server, "tcp://*:5555");

cout<<endl;
cout<<"::::::::::::::::Dealer is Runnig: port:5555::::::::::::::::"<<endl;
cout<<endl;


zloop_t *loop = zloop_new();
zloop_reader(loop,server,&handler,dealer);
zloop_start(loop);
zsock_destroy(&server);


return 0;    
}
