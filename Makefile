CC=g++ -std=c++11
CA= -std=c++11

ZMQ=/usr/local
BOOST=/usr/local

ZMQ_INCS=$(ZMQ)/include
ZMQ_LIBS=$(ZMQ)/lib


BOOTS_INCS=$(BOOST)/boost_1_57_0
BOOST_LIBS=$(BOOST)/lib


all: sudoku sudoku-test heap dealer

sudoku: sudoku.cc
	$(CC) -o sudoku sudoku.cc format.cc


sudoku-test: sudoku-test.cc
	$(CC) -o sudoku-test sudoku-test.cc format.cc
	
heap: sudoku-test.cc
	$(CC) -o heap heap.cc
	
dealer: dealer.cc
	$(CC) -I$(ZMQ_INCS) -L$(ZMQ_LIBS) -o dealer dealer.cc -lczmq -lzmq format.cc
	
worker: worker.cc
	$(CC) -I$(ZMQ_INCS) -L$(ZMQ_LIBS) -o worker worker.cc -lczmq -lzmq format.cc
	
demo: demo.cpp
	g++ -std=c++11 -I/usr/include/  demo.cpp -o demo
	
	