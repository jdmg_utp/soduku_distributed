    #include <czmq.h>
    
    #include <iostream>     // std::cout
    #include <algorithm>    // std::make_heap, std::pop_heap, std::push_heap, std::sort_heap
    #include <vector> 
    #include <set>
    #include <iostream>
    #include <utility>
    #include "format.h"
    #include <sstream>
    #include <stdio.h>
    #include <stdlib.h>
    
    
    using std::vector;
    using std::pair;
    using std::string;
    using std::cout;
    using std::cin;
    using std::endl;
    using std::set;
    using fmt::print;
    
    using Cell = set<int>;
    using Coordinate = pair<size_t, size_t>;
    
    string intToS(int n)
    {
    std::ostringstream sin;
    sin << n;
    std::string val = sin.str();
    return val;
    }
    
    
    struct Statistics {
    size_t solutions;
    size_t failures;
    size_t decisions;
    size_t reductions;
    
    Statistics()
      : solutions(0)
      , failures(0)
      , decisions(0)
      , reductions(0) {}
    void print() const {
    fmt::print_colored(
        fmt::GREEN,
        "Solutions: {}\t Failures: {}\t Decisions: {}\t Reductions: {}\n",
        solutions, failures, decisions, reductions);
    }
    };
    
    
    ////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////   SUDOKU CLASS  //////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    class Sudoku {
    private:
    using Board = vector<vector<Cell>>;
    
    Board board;
    
    public:
    Sudoku() {
    Cell all{1, 2, 3, 4, 5, 6, 7, 8, 9};
    board = {
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
        {all, all, all, all, all, all, all, all, all},
    };
   }
    
    Sudoku(string sk){
    std::string::iterator it;
    int i=0;
    int j=0;
    int k=1;
    Cell c;
    vector <Cell> col;
    Board b;
    for(it=sk.begin(); it!= sk.end(); it++){
        
        char space='\n';
        char number=*it;
        
      
        if(space==number){// nueva celda
           col.push_back(c);
           c.erase(c.begin(),c.end());
           
           if(k%9==0){ //nueva fila
             b.push_back(col);
             col.erase(col.begin(), col.end());
             i++;
           }
           
           k++;
        }else{//es un digito
          int b=number - '0'; //De esta forma se puede obtener un digito si number es u caracter
          c.insert(b);
        }
      }
    
    board=b;
   }
    
    
    /*
    * Ininicializa el soduku si la casilla es igual a 0 coloca un set de datos all{1,2,3,4,5,7,8,9}
    */
    
    Sudoku(const vector<vector<int>>& s) {
    Cell all{1, 2, 3, 4, 5, 6, 7, 8, 9};
    for (const auto& row : s) {
      board.push_back(vector<Cell>());
      for (const auto& v : row) {
        if (v == 0)
          board.back().push_back(all);
        else
          board.back().push_back({v});
      }
    }
    }
    
    
    
    
    Sudoku(const Sudoku& other)
      : board(other.board) {}
    
    /*
    * El sudoku esta resuelto si tiene sola una casilla asignada
    */
    bool isSolved() const {
    for (const auto& row : board)
      for (const auto& cell : row)
        if (cell.size() > 1) return false;
    return true;
    }
    
    /*
    *Es fallido el sudoku si por lo menos una celda no tiene asignado un numero
    */
    
    bool isFailed() const {
    for (const auto& row : board)
      for (const auto& cell : row)
        if (cell.size() == 0) return true;
    return false;
    }
    
    /*
    *Retorna si una celda esta resuelta si tiene un solo numero asignado
    */
    bool solvedCell(size_t i, size_t j) const { return board[i][j].size() == 1; }
    
    
    int valueCell(size_t i, size_t j) const {
        assert(solvedCell(i, j));
        return *(board[i][j].begin());
        }
        
        /*
        * Se encarga de quitar todos los valores repetidos en la fila, columna, y  en subregion donde se encuentra la celda.
        *Esto lo hace hasta cuando todas las celdas pudieran reducir al menos un valor
        */
        
        void reduce() {
        bool r = true;
        while (r) {
          r = false;
          for (size_t x = 0; x < 9; x++)
            for (size_t y = 0; y < 9; y++) {
              r = r || reduceRow(x, y);
              r = r || reduceCol(x, y);
              r = r || reduceBox(x, y);
            }
          // fmt::print_colored(fmt::YELLOW, "Finished pass\n");
        }
    }
    
    
    /*
    * Encuentra la siguiente casilla a resolver, seria la primer casilla que no este resuelta.
    */
    Coordinate nextCellTosolve() const {
      for (size_t x = 0; x < 9; x++)
        for (size_t y = 0; y < 9; y++)
          if (!solvedCell(x, y)) return {x, y};
      assert(false);
      
      return {0, 0};
    }
    
    /*
    * Elige la primera casilla que no este resuelta y la que tenga menos casillas por resolver
    */
    Coordinate smarterNextCellTosolve() const {
      size_t minCard = 9;
      Coordinate result{0, 0};
      for (size_t x = 0; x < 9; x++)
        for (size_t y = 0; y < 9; y++) {
          if (!solvedCell(x, y) && board[x][y].size() < minCard) {
            minCard = board[x][y].size();
            result = {x, y};
          }
        }
      return result;
    }
    
    /*
    * Toma el -primer- valor del set de datos para resolverlo 
    */
    int possibleValueForCell(const Coordinate& c) const {
      return *(board[c.first][c.second].begin());
    }
    
    
    void removeValueForCell(const Coordinate& c, int v) {
      board[c.first][c.second].erase(v);
    }
    
    /*
    * retorna todos los valores de una celda
    */
    Cell getCell(const Coordinate& c){ 
      return board[c.first][c.second];
    }
    
    /*
    *Asigna a un valor en una coordena(celda) dada.
    */
    
    void assignValueForCell(const Coordinate& c, int v) {
    board[c.first][c.second] = {v};
    }
    
    private:
    /**
    * Reduces the cell (i,j) using the row property.
    */
    bool reduceRow(size_t i, size_t j) {
    auto oldSize = board[i][j].size();
    for (size_t x = 0; x < 9; x++) {
      if (x != j && solvedCell(i, x)) {
        // fmt::print_colored(fmt::BLUE, "using {} {} to reduce {} {}\n", i, x,
        // i,j);
        board[i][j].erase(valueCell(i, x));
      }
    }
    auto newSize = board[i][j].size();
    if (newSize < oldSize) {
      // fmt::print_colored(fmt::RED, "Effective row reduction of ({} {})\n", i,
      //                    j);
      return true;
    }
    return false;
    }
    
    /**
    * Reduces the cell (i,j) using the column property.
    */
    bool reduceCol(size_t i, size_t j) {
    // fmt::print_colored(fmt::RED, "Reducing cell ({},{})\n", i, j);
    auto oldSize = board[i][j].size();
    for (size_t row = 0; row < 9; row++) {
      if (row != i && solvedCell(row, j)) {
        board[i][j].erase(valueCell(row, j));
        // fmt::print("I will use {} {}\n", row, j);
      }
    }
    auto newSize = board[i][j].size();
    if (newSize < oldSize) {
      // fmt::print_colored(fmt::RED, "Effective col reduction of ({} {})\n", i,
      //                    j);
      return true;
    }
    return false;
    }
    
    bool reduceBox(size_t i, size_t j) {
    auto oldSize = board[i][j].size();
    
    size_t boxStartRow = i - (i % 3);
    size_t boxStartCol = j - (j % 3);
    
    for (size_t row = 0; row < 3; row++)
      for (size_t col = 0; col < 3; col++) {
        size_t x = row + boxStartRow;
        size_t y = col + boxStartCol;
        if (!(x == i && y == j) && solvedCell(x, y)) {
          board[i][j].erase(valueCell(x, y));
        }
      }
    auto newSize = board[i][j].size();
    if (newSize < oldSize) {
      // fmt::print_colored(fmt::RED, "Effective box reduction of ({} {})\n", i,
      //                    j);
      return true;
    }
    return false;
    }
    
    public:
    void print() const {
    fmt::print("Sudoku\n");
    size_t i = 0;
    for (const auto& row : board) {
      size_t j = 0;
      if (i % 3 == 0) fmt::print("{:-^13}\n", "");
      for (const auto& cell : row) {
        if (j % 3 == 0) fmt::print("|");
    
        if (cell.size() == 0) {
          fmt::print_colored(fmt::RED, "{{}}");
        } else if (cell.size() == 1) {
          auto elem = *(cell.cbegin());
          fmt::print_colored(fmt::GREEN, "{}", elem);
        } else {
          fmt::print("{{");
          for (auto i : cell) fmt::print("{},", i);
          fmt::print("}}");
        }
        j++;
      }
      fmt::print("|\n");
      i++;
    }
    fmt::print("{:-^13}\n", "");
    }
    
    ///////////////////////////////SERIALIZER/////////////////////////////////////
    string serializer()const{
      string str="";         
      
      for(int i=0; i<9;i++){
        for(int j=0; j<9; j++){
		 Cell c=board[i][j];
		 
         
          for(std::set<int>::iterator it=c.begin(); it!= c.end(); it++){
            //int value= *it; //no necesito la representación numerica ya que esta es de tamañano de 32bits            
			char *msg;
			msg=(char*) malloc (sizeof(char));
			sprintf(msg, "%d", *it);
            str.push_back(msg[0]);
            free (msg);
          } 
          
          str.push_back('\n');
        }
      }
      
      return str;
    }
    /////////////////////////////////////////////////////////////////////////////
    
    Board getBoard(){
      return board;
    }
    
    
    };
    
   /*
     ____________:::::::::::::::__END CLASS SUDOKU__:::::::::::::::_____________
    */
    
    
    
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    
    class process{
    private:
      int priority;
      int val;
      Cell valuesLastCell; //Set de valores que se van a enviar
      Coordinate coordinate;
      Sudoku sudoku;
      bool finished_process;
      int result; //indica que tipo de respuesta dio la solción parcial del sudoku en una casilla determinada.
    
    
    public:
    process(void){
     priority=0;
     val=-1;
     coordinate={0, 0};
     finished_process=false;
    }
    
    process(Sudoku _s){
     priority=0;
     val=-1;
     coordinate={0, 0};
     finished_process=false;
     sudoku=_s;
    }
    
    
    process(int _priority,int _val,Coordinate _c, Sudoku _s){
       priority=_priority;
       val=_val;
       coordinate=_c;
       sudoku=_s;
       //
       finished_process=false;
    }
    
    ///////////////////////////////////METHODS/////////////////////////////////////////
    
        pair<Sudoku, int> solveOneProcess(Sudoku& s,Statistics& st, int &val , Coordinate &next) {
                s.reduce();
                st.reductions++;
              
                if (s.isFailed()) {
                  st.failures++;
                  //s.print();
                  return {s, 0};
                } else if (s.isSolved()) {
                  st.solutions++;
                  return {s, 1};
                } else {
              
                  st.decisions++;
                  
                  Sudoku copy(s);

                  next = copy.smarterNextCellTosolve();
                  valuesLastCell=copy.getCell(next);
                  val = copy.possibleValueForCell(next);
                  copy.assignValueForCell(next, val);
                 
                  return {copy, 2};
    
              }
        }
    
        void solve() {
          Statistics st;
          Sudoku root(sudoku);
          cout<<"///////////////////////////////////////////////////////////////"<<endl;
          cout<<"priority:"<<priority<<endl;
          cout<<"__________________________INFO RECIVE____________________________"<<endl;
          sudoku.print();
          cout<<"_______________________________________________________________"<<endl;
         
         
          pair<Sudoku, int> sol =solveOneProcess(root,st,val,coordinate);//parametros que ya estan como propiedades de la clase: val, coordinate
          sudoku=sol.first;
          result=sol.second;
          valuesLastCell.erase(val);// Se borra el valor en set de celdas anteriores a la solución asignada.
          finished_process=true;
          
          
          cout<<"__________________________INFO SEND____________________________"<<endl;
          
          if (result==2){
            cout<<"                 <<<No solution found>>>"<<endl;
          }else if (result==1){
            cout<<"                   <<<Solution found>>>"<<endl;
          } else if(result==0) {
             cout<<"                     <<<failed>>>"<<endl;
          }
          
          cout<<"Celda:"<<coordinate.first<<";"<<coordinate.second<<endl;
          cout<<"valor:"<<val<<endl;
          cout<<"valuesLastCell:";
          printValuesCell();
          
          sudoku.print();
          st.print();
          
          cout<<"_______________________________________________________________"<<endl;
          cout<<"///////////////////////////////////////////////////////////////"<<endl;
        
        }
        
    //////////////////////////// SERIALIZER  CELL////////////////////////////////
    
       Cell deserializerLastCell(string cell_in){
          Cell cell_out;
          std::string::iterator it;
          
          for (it=cell_in.begin(); it!=cell_in.end(); it++){
                char number=*it; 
                int d=number - '0'; //De esta forma se puede obtener un digito si number es u caracter
                cell_out.insert(d);
          }
          
          return cell_out;
        }
        
        string serializerLastCell(){
          
          Cell cell_in=valuesLastCell;
          string cell_out;
          std::set<int>::iterator it;
          
          for (it=cell_in.begin(); it!=cell_in.end(); it++){
				char *c;
				c=(char*) malloc (sizeof(char));
				sprintf(c, "%d", *it);
				cell_out.push_back(c[0]);
				free (c);              
          }
          
          return cell_out;
        }
    
    //////////////////////////////GETs AND SETs/////////////////////////////////
          int getResult(){
            return result;
          }
          
          int getVal(){
            return val;
          }
          
          Cell getValuesLastCell(){
            return valuesLastCell;
          }
          
          void printValuesCell(){
            Cell cell_in=valuesLastCell;
            string cell_out;
            std::set<int>::iterator it;
          
           cout<<"{";
            for (it=cell_in.begin(); it!=cell_in.end(); it++){
                cout<<*it<<",";
            }
            cout<<"}"<<endl;
          }
          
          Coordinate getCoordinate(){
            return coordinate;
          }
          
          Sudoku getSudoku(){
            return sudoku;
          }
          
          void setSudoku(Sudoku _s){
             sudoku=_s;
          }
          
          bool isFinished(){
            return finished_process;
          }
          
          int getPriority(){
            return priority;
          }
          
          void setPriority(int pr){
            priority=pr;
          }
          
          bool getFinished_process(){
            return finished_process;
          }
          
          void setFinished_process(bool val){
            finished_process=val;
          }
          
    };
    /*
    ///////////////////////////////////END CLASS PROCESS//////////////////////////
    */
    
    class worker{
    private:
      void *worker_socket;
      int idWorker;
      process currentProcess;
    
    public:
        worker(void):idWorker(-1){}
        
        worker(void *socket){
           idWorker=-1;
           worker_socket=socket;
        }
        
        worker(int _idWorker){
          idWorker=_idWorker;
        }
        
        
        void  setCurrentProcess(process _p){
          currentProcess=_p;
        }
        
        process getCurrentProcess(){
          return currentProcess;
        }
        
        void sendMsg(void* channel, vector<string> parts) {
          zmsg_t* msg = zmsg_new();
          for (const string& s : parts) {
            zmsg_addstr(msg, s.c_str());
          }
          zmsg_send(&msg, channel);
        }
        
        
        /////////////////////////////ACTIONS/////////////////////////////
        
        void actionJoin(){
        string name_action="join";
        string idWorker_s=intToS(idWorker);
        sendMsg(worker_socket,{name_action,idWorker_s});
        }
        
        void actionSendProcess(){
          string name_action="finished_process";
          
          string idW=intToS(idWorker);
          string priority_out=intToS(currentProcess.getPriority());
          string sudoku_s_out=currentProcess.getSudoku().serializer();
          
          string result_s_out=intToS(currentProcess.getResult());
          
          string val_out =intToS(currentProcess.getVal());
          string valuesLastCell=currentProcess.serializerLastCell();
          Coordinate c= currentProcess.getCoordinate();
          string coordinate_x_out=intToS(c.first);
          string coordinate_y_out=intToS(c.second);
          
         
          
          if(currentProcess.getResult()!=0){ //Si el resultado no es fallido no envia nada, (puede que ya este resuelto o no)
            sendMsg(worker_socket,{name_action,idW,priority_out,sudoku_s_out,result_s_out,val_out,valuesLastCell,coordinate_x_out, coordinate_y_out});
          }else{
            sendMsg(worker_socket,{"failed_process",idW});
          }
          
        }
        
        void actionSolveProcess(zmsg_t* msg){
          
          idWorker=atoi(zmsg_popstr(msg));
          int priority_in=atoi(zmsg_popstr(msg));
          string sudoku_s_in=zmsg_popstr(msg);
          int val_in= atoi(zmsg_popstr(msg));
          
          int  coordinate_x_in= atoi(zmsg_popstr(msg));
          int  coordinate_y_in= atoi(zmsg_popstr(msg));
          bool finished_process= (atoi(zmsg_popstr(msg)))? true: false;
          
          Coordinate c={coordinate_x_in,coordinate_y_in};
          Sudoku s(sudoku_s_in);
          
          process prs(priority_in,val_in, c, s);
          prs.solve();
          setCurrentProcess(prs);
        
        }
        
        void actionSolutionFound(zmsg_t* msg ){
           int idWorkerFound = atoi(zmsg_popstr(msg));
           cout<<"-------------------<Solution Found in worker id:"<<idWorkerFound<<" >--------------------------"<<endl;
        }
    
    //////////////////////////////////////////////////////////////////
    
    };
    
    
    void handler( zmq_pollitem_t* items,void* worker_socket,worker* wrk){
    
        while(true){
            //int x;
            //cin >> x;
            int st = zmq_poll(items, 1, 10 * ZMQ_POLL_MSEC);
            
            if (st == -1) {
                 break;// Handles termination by ^C
            }
             
            if (items[0].revents & ZMQ_POLLIN) {
                zmsg_t* msg = zmsg_recv(worker_socket);
             
                zframe_t* action = zmsg_pop(msg);
                
              if(zframe_streq(action,"solveProcess")){
                  wrk->actionSolveProcess(msg);
                  wrk->actionSendProcess();
               }else if(zframe_streq(action,"solutionFound")){
                 wrk->actionSolutionFound(msg);
                 zmsg_destroy(&msg);
                 break;
               }
                
              
              zmsg_destroy(&msg);
            }
        
        }
    }
    
    
    
    
    
    int main (int argc, char** argv){
      zctx_t* context = zctx_new();
      void* worker_socket = zsocket_new(context, ZMQ_DEALER);
      zsocket_connect(worker_socket, "tcp://localhost:5555");
      zmq_pollitem_t items[] = {{worker_socket, 0, ZMQ_POLLIN, 0}};
      
      
      worker* wrk= new worker(worker_socket);
      wrk->actionJoin();
      handler(items,worker_socket,wrk);
      
      
      
      zctx_destroy(&context);  
      return EXIT_SUCCESS;
    }
