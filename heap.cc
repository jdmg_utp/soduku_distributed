#include <iostream>     // std::cout
#include <algorithm>    // std::make_heap, std::pop_heap, std::push_heap, std::sort_heap
#include <vector>       // std::vector
#include <set>
#include <string>

using std::make_heap;
using std::push_heap;
using std::pop_heap;
using std::sort_heap;
using std::cout;
using std::vector;
using std::set;
using std::pair;
using std::string;

class data{
  private:
  int val;
  public:

  
  data(int d){
    val=d;
  }
  
  int getVal(){
    return val;
  }
};

/*
bool operator <(const data& lhs, const data& rhs){
  return true;
}
*/

class BinaryMinHeap {
private:
      int *data;
      int heapSize;
      int arraySize;
 
      int getLeftChildIndex(int nodeIndex) {
            return 2 * nodeIndex + 1;
      }
 
      int getRightChildIndex(int nodeIndex) {
            return 2 * nodeIndex + 2;
      }
 
      int getParentIndex(int nodeIndex) {
            return (nodeIndex - 1) / 2;
      }
 
public:
      BinaryMinHeap(int size) {
            data = new int[size];
            heapSize = 0;
            arraySize = size;
      }    
 
      int getMinimum() {
            if (isEmpty())
                  throw string("Heap is empty");
            else
                  return data[0];
      }
 
      bool isEmpty() {
            return (heapSize == 0);
      }

 
      ~BinaryMinHeap() {
            delete[] data;
      }
};


int main () {
  data d(3);
  data e(5);
  data f(8);
  data g(1);
  data h(1);
  
  /*
  vector <pair<int,data>> v;
  

  make_heap (v.begin(),v.end());
  
  v.push_back({d.getVal(),d});
  push_heap (v.begin(),v.end());
  
  v.push_back({e.getVal(),e});
  push_heap (v.begin(),v.end());
  
  v.push_back({f.getVal(),f});
  push_heap (v.begin(),v.end());
  
  v.push_back({g.getVal(),g});
  push_heap (v.begin(),v.end());
  v.push_back({g.getVal(),g});
  push_heap (v.begin(),v.end());
  
  cout<<v.front().first<<"\n";
  
  for (unsigned i=0; i<v.size(); i++)
        std::cout << ' ' << v[i].first;
  */
  
  /*
  cout << "initial max heap   : " << v.front() << '\n';

  pop_heap (v.begin(),v.end()); 
  v.pop_back();
  std::cout << "max heap after pop : " << v.front() << '\n';

  v.push_back(3);
  std::push_heap (v.begin(),v.end());
  cout << "max heap after push: " << v.front() << '\n';

  sort_heap (v.begin(),v.end());

  cout << "final sorted range :";
  for (unsigned i=0; i<v.size(); i++)
        cout << ' ' << v[i];
   cout << '\n';
   
  */
  /*
  data d(2);
  data e(5);
  data f(3);
  data g(8);
  data h(7);
  //
  
  vector <data> dates;
  dates.push_back(d);
  dates.push_back(e);
  
  set<data> de;
  //de.push(e);
  */
  
 
  

  return 0;
}