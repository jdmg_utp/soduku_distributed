#include <iostream>     // std::cout
#include <algorithm>    // std::make_heap, std::pop_heap, std::push_heap, std::sort_heap
#include <vector>       // std::vector
#include <set>
#include <string>

using std::make_heap;
using std::push_heap;
using std::pop_heap;
using std::sort_heap;
using std::cout;
using std::vector;
using std::set;
using std::pair;
using std::string;
using std::endl;

class Foo {
private:
public:
  string  x;
  
  Foo(string _x){
  	x=_x;
  }
  
  
};

template <typename size_t, typename Foo>
bool comparePairs(const std::pair<int,Foo>& lhs, const std::pair<int,Foo>& rhs)
{
  return lhs.first <= rhs.first;
}

  
int main() {
	
	cout<<"Heaps"<<endl;
	
	vector<pair<size_t,Foo>> heap;
	
	//cout<<heap[0].first<<endl;
	
	make_heap(heap.begin(), heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({10,Foo("hola")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({7,Foo("como")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({14,Foo("estas")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({2,Foo("tu")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({6,Foo("en")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({8,Foo("dia")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({9,Foo("feliz")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({9,Foo("feliz2")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
	
	heap.push_back({5,Foo("feliz2")});
	push_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);

	
	//std::sort_heap (heap.end(),heap.begin(),comparePairs<int,Foo>);
	
	for(auto e : heap) 
	    cout<<e.first<<endl;
	cout << " ---------------- " << endl;
	
	
	while(!heap.empty()) {
		auto e = heap.front();
		cout<<e.first<<":"<<e.second.x<<endl;
		pop_heap(heap.begin(),heap.end(),comparePairs<int,Foo>);
		heap.pop_back();
	}
	
	return 0;
}
